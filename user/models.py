from django.db import models
from django.contrib.auth.models import User

# Create your models here. 
class Geos(models.Model):
    lat = models.FloatField()
    lng = models.FloatField()

class Adress(models.Model):
    street  = models.TextField()
    suite   = models.TextField()
    city    = models.TextField()
    zipcode = models.TextField()
    geos    = models.ForeignKey(Geos, on_delete=models.CASCADE)

class Company(models.Model):
    name         = models.TextField()
    catch_phrase = models.TextField()
    bs           = models.TextField()

class Profile(models.Model):
    user     = models.OneToOneField(User, on_delete=models.CASCADE)
    phone    = models.TextField()
    adress   = models.ForeignKey(Adress, on_delete=models.CASCADE)
    company  = models.ForeignKey(Company, on_delete=models.CASCADE)
