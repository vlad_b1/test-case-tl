from django.contrib import admin

# Register your models here.
from .models import Profile, Adress, Geos, Company

admin.site.register(Profile)
admin.site.register(Adress)
admin.site.register(Geos)
admin.site.register(Company)
