"""
Отвечает за перезачу  html страницы на рендер, а также за передачу странице данных.
"""

from django.http import HttpResponse
from django.shortcuts import render

from user.models import Profile, Adress, Geos, Company
from post.models import Post

from .data_provider import populate_user, populate_post

# Create your views here.
def index_view(request, *args, **kwargs):
    # Методы заполнения таблиц значениями. (скорее всего плохое место для размещения данного кода, на за неимением/незнанием 
    # альтернативных точех входа для выполнения при старте сервера расположил здесь)
    populate_user()
    populate_post()
    # Получает список всех постов.
    posts = Post.objects.all()
    return render(request, "index.html", {"posts": posts})
