"""
Содержит логику полученя данных для заполнения базы данных из внешних источников. (поенциаль но самое правильное место, для
размещения данного типа кода)
"""

from django.http import HttpResponse
import requests
import json

from django.contrib.auth.models import User
from user.models import Profile, Adress, Geos, Company
from post.models import Post

user_http = "http://jsonplaceholder.typicode.com/users"
post_http = "http://jsonplaceholder.typicode.com/posts"

def get_html_content(req_http):
    """
    Метод получения json содержимого по http запросу.
    """

    USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36"
    LANGUAGE = "en-US,en;q=0.5"
    session = requests.Session()
    session.headers['User-Agent'] = USER_AGENT
    session.headers['Accept-Language'] = LANGUAGE
    session.headers['Content-Language'] = LANGUAGE

    html_content = requests.get(req_http)
    json_response = html_content.json()
    return json_response

def gen_password():
    return "12345678"

def populate_user():
    User.objects.all().delete()
    Profile.objects.all().delete()
    Geos.objects.all().delete()
    Company.objects.all().delete()

    results = get_html_content(user_http)

    for result in results:
        adress = result["address"]
        geo = adress["geo"]

        geo_obj = Geos.objects.create(lat=geo["lat"], lng=geo["lng"])
        address_obj = Adress.objects.create(street=adress["street"], suite=adress["suite"], city=adress["city"], zipcode=adress["zipcode"], geos=geo_obj)

        company = result["company"]

        company_obj = Company.objects.create(name=company["name"], catch_phrase=company["catchPhrase"], bs=company["bs"])

        # 0: neme, 1: surname
        name_schema = result["name"].split(' ')

        user_obj = User.objects.create(is_superuser=False, username=result["username"], first_name=name_schema[0], last_name=name_schema[1],
            email=result["email"], is_staff=False, password=gen_password(), is_active=True, id=result["id"])

        Profile.objects.create(user=user_obj, phone=result["phone"], adress=address_obj, company=company_obj)

def populate_post():
    Post.objects.all().delete()

    results = get_html_content(post_http)

    for result in results:
        # ищет по user.id, внутреннее представление user_id
        user_obj = Profile.objects.get(user_id=result["userId"])
        Post.objects.create(id=result["id"], user=user_obj, title=result["title"], body=result["body"])
