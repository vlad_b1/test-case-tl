from django.db import models
from user.models import Profile

# Create your models here.
class Post(models.Model):
    id      = models.IntegerField(primary_key=True)
    user    = models.ForeignKey(Profile, on_delete=models.CASCADE)
    title   = models.TextField()
    body    = models.TextField()
